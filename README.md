# TTM00-FW-PIC32-Training-V1

## 1. Description

This project is an introduction to firmware, GIT management, version control and the use of the PIC32. 

## 2. Hardware description

- PIC32MX470512H: [datasheet](https://ww1.microchip.com/downloads/en/DeviceDoc/PIC32MX330350370430450470_Datasheet_DS60001185H.pdf)
- User guide: [curiosity board](https://ww1.microchip.com/downloads/en/DeviceDoc/70005283B.pdf)

## 3. Serial commands

## 4. Prerequisites
> ### 1. SDK Version
>> - IDE version: MPLAB v5.50
>> - Compiler version: XC32 v1.42
>> - Project configuration:
>>> - Categories : Microchip Embedded 
>>> - Projects : Standalone Project 
>>> - Family : All families 
>>> - Device : PIC32MX470F512H 
>>> - Hardware tools : Microchip starter kits 
>>> - Compiler : XC32 (VI .42) 
>>> - Encoding : ISO-8859-1 

## 5. Versioning

> ### 1. Current version of the FW
>> - V1.0.20210713

## 6. Authors
> 1. project staff: Esteban Naranjo Morales
> 2. Maintainer contact email: enaranjom@unal.edu.co
